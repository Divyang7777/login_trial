import React from 'react'
import logo from './logo.svg';
import './App.css';


export class LandingPage extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            isSignedIn: false,
            showLoginButton: true,
            userId: '',
            userFullName: '',
            userName: '',
            userEmail: '',
            userImageUrl: ''
        }
    }

    onSuccess(event) {
        debugger
        this.setState({
            isSignedIn: true
        })
    }

    componentDidMount() {
        let { onSuccess } = this

        window.gapi.load('auth2', () => {
            this.auth2 = window.gapi.auth2.init({
                client_id: '151687904539-0lbfab1f6o38irvs5aj55i09d6t1pnmf.apps.googleusercontent.com',
            }).then(res => {
                if (res && res.isSignedIn.get()) {
                    let profile = res.currentUser.get().getBasicProfile();
                    let details = {
                        userId: profile.getId(),
                        userFullName: profile.getName(),
                        userName: profile.getGivenName(),
                        userEmail: profile.getEmail(),
                        userImageUrl: profile.getImageUrl()
                    }
                    this.props.history.push('/dashboard', { details: details, signOut: res })
                    // this.getSetProfile(profile)
                } else {
                    this.setState({ showLoginButton: true })
                }
            })
        })

        window && window.gapi && window.gapi.load('signin2', function () {
            window.gapi.signin2.render('loginButton', {
                width: 200,
                height: 50,
                onSuccess: onSuccess.bind(this),
            })
        })
    }

    getSetProfile(profile) {
        this.setState({
            showLoginButton: false,
            isSignedIn: true,
            userId: profile.getId(),
            userFullName: profile.getName(),
            userName: profile.getGivenName(),
            userEmail: profile.getEmail(),
            userImageUrl: profile.getImageUrl()
        })
    }


    getContent() {
        let { showLoginButton, userFullName, userName, userEmail, userImageUrl } = this.state
        if (this.state.isSignedIn) {
            return (
                <div>
                    <p>Hello <b>{userFullName}</b>, <br /><br /> you're signed in as <b>{userName}</b> <br /> your Email is <b>{userEmail}</b> </p>
                </div>
            )
        } else {
            return (
                <div>
                    <p>You are not signed in. Click here to sign in.</p>
                    {showLoginButton ? <button id="loginButton" onClick={() => {
                        debugger

                        this.auth2.then(res => {
                            debugger

                            if (res.isSignedIn.get()) {
                                debugger
                            }
                        })
                    }} >Login with Google</button> :
                        <button onClick={() => {
                            this.auth2.then(res => {
                                var profile = res.currentUser.get().getBasicProfile();
                                this.getSetProfile(profile)
                            })
                        }}  >Show Profile</button>}
                </div>
            )
        }

    }

    render() {
        return (
            <div className="App" >
                <header className="App-header">
                    <img src={logo} className="App-logo" alt="logo" />
                    {this.getContent()}
                </header>
            </div>
        )
    }
}