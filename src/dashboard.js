
import React from 'react'
import logo from './logo.svg';
import './App.css';


export class Dashboard extends React.Component {
    constructor(props) {
        super(props);
        props.location && !props.location.state && props.history.pop();
        this.state = {
            details: props.location.state.details
        }
    }

    signOut() {
        // In WIP !!!
        this.props.history.pop();
        window.gapi.load('auth2', () => {
            // props.location.state.details
            let auth2 = window.gapi.auth2.getAuthInstance()
            auth2.signOut().then(function () {
                console.log('User signed out.');
            });
        })
    }

    render() {
        const { details } = this.state
        return (
            <div className="App" >
                <header className="App-header">
                    <img src={logo} className="App-logo" alt="logo" />
                    <div>
                        <p>Hello
                        <b>{details.userFullName}</b>, <br /><br />
                        you're signed in as <b>{details.userName}</b> <br />
                        your Email is <b>{details.userEmail}</b> </p>
                    </div>
                    <button onClick={() => this.signOut()}> Sign out</button>
                </header>
            </div>
        )
    }

}